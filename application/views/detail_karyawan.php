<?php
	foreach($detail_karyawan as $data) 
	{
    	$nik			= $data->nik;
		$nama_lengkap	= $data->nama_lengkap;
		$tempat_lahir	= $data->tempat_lahir;
		$tgl_lahir		= $data->tgl_lahir;
		$jenis_kelamin	= $data->jenis_kelamin;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
		$kode_jabatan	= $data->kode_jabatan;
    }
?>
<body bgcolor="#999999">
<center><font color="#FFFFFF" size="+3" face="Arial, Helvetica, sans-serif">Detail Karyawan</font></center><br/>
<table border="0" cellpadding="8" align="center" width="30%" bgcolor="#FFFFFF">
<tr>
	<td>NIK </td>
    <td>:</td>
    <td><?= $nik;?></td>
</tr>
<tr>
    <td>Nama Karyawan </td>
    <td>:</td>
    <td><?= $nama_lengkap;?></td>
</tr>
<tr>
    <td>Tempat Lahir </td>
    <td>:</td>
    <td><?= $tempat_lahir;?></td>
</tr>
<tr>
    <td>Tanggal Lahir </td>
    <td>:</td>
    <td><?= $tgl_lahir;?></td>
</tr>
<tr>
    <td>Jenis Kelamin </td>
    <td>:</td>
    <td><?= $jenis_kelamin;?></td>
 </tr>
 <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><?= $alamat;?></td>
  </tr>
  <tr>
    <td>Telp</td>
    <td>:</td>
    <td><?= $telp;?></td>
</tr>
<tr>
    <td>Kode Jabatan</td>
    <td>:</td>
    <td><?= $kode_jabatan;?></td>
</tr>
</table>
</body>