<link rel="stylesheet" href="<?=base_url();?>/assets/style.css" type="text/css" media="screen" />
<body bgcolor="#999999">

<h1 align="center" ><font color="#FFFFFF" face="Trebuchet MS, Arial, Helvetica, sans-serif">Toko Jaya Abadi</font></h1>


<header class="header">
	<div class="menu-malasngoding">

		<ul>
			<li><a href="<?=base_url();?>home">Home</a></li>
			<li><a href="#">Laporan</a></li>
			<li class="dropdown"><a href="#">Data</a>
				<ul class="isi-dropdown">
					<li><a href="<?=base_url();?>karyawan/listkaryawan">Karyawan</a></li>
					<li><a href="<?=base_url();?>jabatan/listjabatan">Jabatan</a></li>
					<li><a href="<?=base_url();?>barang/listbarang">Barang</a></li>
					<li><a href="<?=base_url();?>jenis_barang/listjenisbarang">Jenis Barang</a></li>
                    <li><a href="<?=base_url();?>supplier/listsupplier">Supplier</a></li>
				</ul>
			</li>
            <li class="dropdown"><a href="#">Transaksi</a>
            <ul class="isi-dropdown">
					<li><a href="pembelian/input_pembelian">Pembelian</a></li>
					<li><a href="#">Penjualan</a></li>
				</ul>
              </li>
			<li><a href="#">Logout</a></li>
		</ul>

	</div>
</header>

<br/>
	<center><font color="#FFFFFF" size="+2" face="Georgia, Times New Roman, Times, serif">Data Jenis Barang</font></center><br/>
	<table  align="center" width="90%" border="1" cellspacing="0" cellpadding="5" bordercolor="#FFFFFF" bgcolor="#FFFFFF">
    
        <tr>
        	<td><font><a href="input_jenis_barang">Input Jenis Barang</a></font></td>
    		<td colspan="6" align="right"><label for="Cari Nama"></label>
    		<input type="text" name="Cari Nama" id="Cari Nama" placeholder="Cari Nama">
    		<input name="cari data" type="button" value="cari data"></td>
  		</tr>
        <tr>
        	<th>No</th>
        	<th>Kode Jenis</th>
            <th>Nama Jenis</th>
            <th>Aksi</th>
        </tr>
        <?php
	  	$no = 0;
	  	foreach ($data_jenis_barang as $data) { $no++;
	  
	 	?>
    	<tr>
    		<td><?=$no;?></td>
            <td><?=$data->kode_jenis;?></td>
            <td><?=$data->nama_jenis;?></td>
            <td><a href="<?= base_url(); ?>jenis_barang/detailJenis_barang/<?= $data->kode_jenis; ?>">Detail</a>
            | 	<a href="<?= base_url(); ?>jenis_barang/editjenis_barang/<?= $data->kode_jenis; ?>">Edit</a>
            |<a onClick="return confirm('Anda Yakin Ingin Hapus Data')"href="<?= base_url();?>jenis_barang/delete/<?= $data->kode_jenis;?>">
       Delete</a> 
  
            </td>
    	</tr>
    	<tr>
    	</tr>
    	<?php } ?>
    </table>
    </body>